package br.ucsal.testequalidade;

/**
 * O uso de métodos de instância/publicos nesta classe é intencional e objetiva
 * ilustrar o uso de mocks para métodos de instância.
 * 
 * @author claudioneiva
 *
 */
public class CalculoEHelper {

	public FatorialHelper fatorialHelper;

	public CalculoEHelper(FatorialHelper fatorialHelper) {
		this.fatorialHelper = fatorialHelper;
	}

	/**
	 * Calcula o valor de E, que é a soma dos inversos dos fatoriais de 0 a n.
	 * 
	 * Exemplo 1
	 * 
	 * Se N = 5, então:
	 *
	 * E = 1 / 0! + 1 / 1! + 1 / 2! + 1 / 3! + 1 / 4! + 1 / 5!
	 * 
	 * @param n número inteiro, maior ou igual à zero.
	 * @return valor E.
	 */
	public Double calcularE(Integer n) { 
		Double e = 0d;
		for (int i = 0; i <= n; i++) {
			Long fatorial = fatorialHelper.calcularFatorial(i);
			e = e + 1d / fatorial;
		}
		return e;
	}

}
