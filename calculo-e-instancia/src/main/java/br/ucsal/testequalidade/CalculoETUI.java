package br.ucsal.testequalidade;

import java.util.Scanner;

/**
 * O uso de métodos de instância/publicos nesta classe é intencional e objetiva
 * ilustrar o uso de mocks para métodos de instância.
 * 
 * @author claudioneiva
 *
 */
public class CalculoETUI {

	private static Scanner scanner = new Scanner(System.in);
	private static CalculoEHelper calculoEHelper = new CalculoEHelper(new FatorialHelper());

	public static void main(String[] args) {
		obterNCalcularExibirE();
	}

	public static void obterNCalcularExibirE() {
		Integer n;
		Double e;

		n = obterNumeroInteiroPositivo();
		e = calculoEHelper.calcularE(n);
		exibirE(n, e);
	}

	public static Integer obterNumeroInteiroPositivo() {
		Integer n;
		do {
			System.out.println("Informe um número maior ou igual a zero:");
			n = scanner.nextInt();
			if (n < 0) {
				System.out.println("Número fora da faixa.");
			}
		} while (n < 0);
		return n;
	}

	public static void exibirE(Integer n, Double e) {
		System.out.println("E(" + n + ")=" + e);
	}

}
